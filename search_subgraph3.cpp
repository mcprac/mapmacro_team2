#include <iostream>
#include <vector>
#include <map>
#include <string>
#include <fstream>
#include <cstdlib>
#include <algorithm>


using namespace std;

enum TYPE
{
    NOT,
    AND,
    OR,
    NOR,
    NAND,
    XOR,
    XNOR,
    IN,
    OUT,
    UNKNOWN
};


class Node
{
public:
    TYPE type;
    int in[2];  // in NOT IN or OUT element must be similar
    vector<int> out;
};

class Graph
{
public:
	vector<Node> element;
	
	Graph(int a){
		element.resize(a);
	}
	Graph(Graph& g){
		element.resize(g.element.size());
		for (int i=0; i<element.size(); i++)
			element[i] = g.element[i];
	}
	
	bool findSubGraph(Graph &subgraph, vector<int> &answer);
	int findNextNodePlace(Node x, int oldPlace, vector<int> &placement);
	void findTopLevel(vector <int> &topLevel);
	bool initPlacement(Graph &subgraph, vector<int> &topLevel, vector<int> &placement);
	bool getPlacement(Graph &subgraph, vector <int> &topLevel, vector <int> &placement);
	bool checkChilds(Graph &subgraph, Node current, Node currentSub,
                 map<int, int> &perm, vector<int> &checked, bool &isDel);
    bool testPlacement(Graph &subgraph, vector <int> &topLevel, vector <int> &placement, vector <int> &ans);
};	

// find nodes, that have connections with IN's
void Graph::findTopLevel(vector <int> &topLevel)
{
    int i, parentIndex1, parentIndex2;
    for (i = 0; i < element.size(); i++) {
        if (element[i].type == IN) {
            continue;
        }
        parentIndex1 = element[i].in[0];
        parentIndex2 = element[i].in[1];
        if (element[parentIndex1].type == IN || element[parentIndex2].type == IN) {
            topLevel.push_back(i);
        }
    }
}

// find next place of the Node current type in graph
int Graph::findNextNodePlace(Node x, int oldPlace, vector<int> &placement)  // find Node place in graph
{
    for (int i = oldPlace + 1; i < element.size(); i++) {
        if (element[i].type == x.type && find(placement.begin(), placement.end(), i) == placement.end()) {
            return i;
        }
    }

    // if we can't find x in [oldPlace + 1, graph.size()]
    for (int i = 0; i < oldPlace; i++) {
        if (element[i].type == x.type && find(placement.begin(), placement.end(), i) == placement.end()) {
            return i;
        }
    }
    return -1;
}


// set initial first posiible place of topLevel
bool Graph::initPlacement(Graph &subgraph, vector<int> &topLevel, vector<int> &placement)
{
    for (int i = 0; i < placement.size(); i++) {
        placement[i] = findNextNodePlace(subgraph.element[topLevel[i]], placement[i], placement);
        if (placement[i] == -1) {
            return false;
        }
    }
    return true;

}

//get next possible placement of topLevel
bool Graph::getPlacement(Graph &subgraph, vector <int> &topLevel, vector <int> &placement)  // generate next place of
                                                                                                                   //topLevel in graph
{
    int i = topLevel.size() - 1, index = -1;
    while (i >= 0) {
        index = findNextNodePlace(subgraph.element[topLevel[i]], placement[i], placement);
        if (index == -1) {
            return false;
        }
        if (index <= placement[i]) {
            i--;
            if (i < 0) {
                break;
            }
            placement[i] = index;
        } else {
            placement[i] = index;
            return true;
        }
    }
    return false;
}


// check list of childs of node current to match list of childs in currentSub in subgraph
bool Graph::checkChilds(Graph &subgraph, Node current, Node currentSub,
                 map<int, int> &perm, vector<int> &checked, bool &isDel)
{
    int j;
    for (j = 0; j < current.out.size(); j++) {
        Node outNode = element[current.out[j]];
        Node outNodeSub = subgraph.element[currentSub.out[j]];
        if (perm.find(outNode.in[0]) != perm.end() && perm.find(outNode.in[1]) != perm.end()) {

            if (outNodeSub.type != OUT && outNode.type != outNodeSub.type) {
                return false;
            }

            if (!(perm[outNode.in[0]] == outNodeSub.in[0] && perm[outNode.in[1]] == outNodeSub.in[1]||
                  perm[outNode.in[0]] == outNodeSub.in[1] && perm[outNode.in[1]] == outNodeSub.in[0])) {
                return false;
            } else {
                if (outNodeSub.type != OUT) {
                    checked.push_back(current.out[j]);
                }
                perm[current.out[j]] = currentSub.out[j];   // add to perm new link
            }
        } else {
            //isDel = false;
            isDel = true;
        }
    }
    return true;
}


// check new placement of topLevel
 bool Graph::testPlacement(Graph &subgraph, vector <int> &topLevel, vector <int> &placement, vector <int> &ans)  // test new placement
 {
    map <int, int> perm;    // index of Node in graph<->subgraph (permutation)
    vector <int> checked(placement.size());  // nodes nedd to check
    int i, j;
    bool isDel;
    for (i = 0; i < topLevel.size(); i++) {
        perm[placement[i]] = topLevel[i];
        checked[i] = placement[i];
    }

    while (checked.size()) {
        for (i = 0; i < checked.size(); i++) {
             Node current = element[checked[i]];
             Node currentSub = subgraph.element[perm[checked[i]]];
             if (current.out.size() != currentSub.out.size()) {
                 return false;
             }
             if (!checkChilds(subgraph, current, currentSub, perm, checked, isDel)) {
                 return false;
             }
             if (isDel) {
                 checked.erase(checked.begin() + i);
                 i--;
             }
        }
    }


    ans.clear();
    ans.resize(subgraph.element.size());
    map<int, int>:: iterator t;

    // set IN's
    Node x, xs;
    for (t = perm.begin(); t != perm.end(); ++t) {
        x = element[t->first];
        xs = subgraph.element[t->second];
        if (subgraph.element[xs.in[0]].type == IN) {
            perm[x.in[0]] = xs.in[0];
        }

        if (subgraph.element[xs.in[1]].type == IN) {
            perm[x.in[1]] = xs.in[1];
        }


    }

    for (t = perm.begin(); t != perm.end(); ++t) {
        ans[t->second] = t->first;
    }
 }

 // find place of subgraph
 // res - vector of indexes, mean that node i in subgraph placed by node res[i] in graph
bool Graph::findSubGraph(Graph &subgraph, vector<int> &answer)
{
    vector <int> topLevel;
    subgraph.findTopLevel(topLevel);
    vector<int> placement(topLevel.size(), -1);
    if (!initPlacement(subgraph, topLevel, placement)) {    // we can't find one of the element in subgraph in graph -> false
        return false;
    }
   do {
        if (testPlacement(subgraph, topLevel, placement, answer)) {
            return true;
        }
    } while (getPlacement(subgraph, topLevel, placement));
    return false;
}

void fillSubGraph(Graph &subgraph){
	
		subgraph.element[0].type = IN;
		subgraph.element[0].in[0] = 0;
		subgraph.element[0].in[1] = 0;
		subgraph.element[0].out.push_back(3);
		
		subgraph.element[1].type = IN;
		subgraph.element[1].in[0] = 0;
		subgraph.element[1].in[1] = 0;
		subgraph.element[1].out.push_back(2);
		
		subgraph.element[2].type = NOT;
		subgraph.element[2].in[0] = 1;
		subgraph.element[2].in[1] = 1;
		subgraph.element[2].out.push_back(3);
		
		subgraph.element[3].type = XOR;
		subgraph.element[3].in[0] = 0;
		subgraph.element[3].in[1] = 2;
		subgraph.element[3].out.push_back(4);
		
		subgraph.element[4].type = OUT;
		subgraph.element[4].in[0] = 3;
		subgraph.element[4].in[1] = 3;
		subgraph.element[4].out.push_back(0);
    	
}

void fillGraph(Graph &graph){
	
		graph.element[0].type = IN;
		graph.element[0].in[0] = 0;
		graph.element[0].in[1] = 0;
		graph.element[0].out.push_back(6);
		
		graph.element[1].type = IN;
		graph.element[1].in[0] = 0;
		graph.element[1].in[1] = 0;
		graph.element[1].out.push_back(4);
		
		graph.element[2].type = IN;
		graph.element[2].in[0] = 0;
		graph.element[2].in[1] = 0;
		graph.element[2].out.push_back(5);
		
		graph.element[3].type = AND;
		graph.element[3].in[0] = 0;
		graph.element[3].in[1] = 1;
		graph.element[3].out.push_back(7);
		
		graph.element[4].type = NOT;
		graph.element[4].in[0] = 1;
		graph.element[4].in[1] = 1;
		graph.element[4].out.push_back(6);
		
		graph.element[5].type = AND;
		graph.element[5].in[0] = 1;
		graph.element[5].in[1] = 2;
		graph.element[5].out.push_back(8);
		
		graph.element[6].type = XOR;
		graph.element[6].in[0] = 0;
		graph.element[6].in[1] = 4;
		graph.element[6].out.push_back(7);
		
		graph.element[7].type = OR;
		graph.element[7].in[0] = 3;
		graph.element[7].in[1] = 6;
		graph.element[7].out.push_back(8);
		graph.element[7].out.push_back(11);
		
		graph.element[8].type = XOR;
		graph.element[8].in[0] = 7;
		graph.element[8].in[1] = 5;
		graph.element[8].out.push_back(9);
		
		graph.element[9].type = NOT;
		graph.element[9].in[0] = 8;
		graph.element[9].in[1] = 8;
		graph.element[9].out.push_back(10);
		
		graph.element[10].type = OUT;
		graph.element[10].in[0] = 9;
		graph.element[10].in[1] = 9;
		graph.element[10].out.push_back(0);
		
		graph.element[11].type = OUT;
		graph.element[11].in[0] = 7;
		graph.element[11].in[1] = 7;
		graph.element[11].out.push_back(0);
	
}



int main(int argc, char *argv[])
{
    Graph graph(12), subgraph(5);
    vector <int> res;
    
    fillGraph(graph);
    fillSubGraph(subgraph);
    
    bool f = graph.findSubGraph(subgraph, res);
    
    if (f){
		cout<<"Find subgraph!"<<endl;
		for (int i = 0; i < res.size(); i++)
			cout<<res[i]<<" ";
		cout<<endl;
	}
	else
		cout<<"Not find subgraph!"<<endl;
    return 0;
}


