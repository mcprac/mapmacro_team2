#include <iostream>
#include <vector>
#include <map>
#include <string>
#include <fstream>
#include <cstdlib>
#include <algorithm>


using namespace std;

enum TYPE
{
    NOT,
    AND,
    OR,
    NOR,
    NAND,
    XOR,
    XNOR,
    IN,
    OUT,
    UNKNOWN
};


class Node
{
public:
    TYPE type;
    int in[2];  // in NOT IN or OUT element must be similar
    vector<int> out;
};

// find nodes, that have connections with IN's
void findTopLevel(vector <Node> &graph, vector <int> &topLevel)
{
    int i, parentIndex1, parentIndex2;
    for (i = 0; i < graph.size(); i++) {
        if (graph[i].type == IN) {
            continue;
        }
        parentIndex1 = graph[i].in[0];
        parentIndex2 = graph[i].in[1];
        if (graph[parentIndex1].type == IN || graph[parentIndex2].type == IN) {
            topLevel.push_back(i);
        }
    }
}

// find next place of the Node current type in graph
int findNextNodePlace(vector <Node> &graph, Node x, int oldPlace, vector<int> &placement)  // find Node place in graph
{
    for (int i = oldPlace + 1; i < graph.size(); i++) {
        if (graph[i].type == x.type && find(placement.begin(), placement.end(), i) == placement.end()) {
            return i;
        }
    }

    // if we can't find x in [oldPlace + 1, graph.size()]
    for (int i = 0; i < oldPlace; i++) {
        if (graph[i].type == x.type && find(placement.begin(), placement.end(), i) == placement.end()) {
            return i;
        }
    }
    return -1;
}


// set initial first posiible place of topLevel
bool initPlacement(vector<Node> &graph, vector<Node> &subgraph, vector<int> &topLevel, vector<int> &placement)
{
    for (int i = 0; i < placement.size(); i++) {
        placement[i] = findNextNodePlace(graph, subgraph[topLevel[i]], placement[i], placement);
        if (placement[i] == -1) {
            return false;
        }
    }
    return true;

}

//get next possible placement of topLevel
bool getPlacement(vector <Node> &graph, vector <Node> &subgraph, vector <int> &topLevel, vector <int> &placement)  // generate next place of
                                                                                                                   //topLevel in graph
{
    int i = topLevel.size() - 1, index = -1;
    while (i >= 0) {
        index = findNextNodePlace(graph, subgraph[topLevel[i]], placement[i], placement);
        if (index == -1) {
            return false;
        }
        if (index <= placement[i]) {
            i--;
            if (i < 0) {
                break;
            }
            placement[i] = index;
        } else {
            placement[i] = index;
            return true;
        }
    }
    return false;
}


// check list of childs of node current to match list of childs in currentSub in subgraph
bool checkChilds(vector<Node> &graph, vector<Node> &subgraph, Node current, Node currentSub,
                 map<int, int> &perm, vector<int> &checked, bool &isDel)
{
    int j;
    for (j = 0; j < current.out.size(); j++) {
        Node outNode = graph[current.out[j]];
        Node outNodeSub = subgraph[currentSub.out[j]];
        if (perm.find(outNode.in[0]) != perm.end() && perm.find(outNode.in[1]) != perm.end()) {

            if (outNodeSub.type != OUT && outNode.type != outNodeSub.type) {
                return false;
            }

            if (!(perm[outNode.in[0]] == outNodeSub.in[0] && perm[outNode.in[1]] == outNodeSub.in[1]||
                  perm[outNode.in[0]] == outNodeSub.in[1] && perm[outNode.in[1]] == outNodeSub.in[0])) {
                return false;
            } else {
                if (outNodeSub.type != OUT) {
                    checked.push_back(current.out[j]);
                }
                perm[current.out[j]] = currentSub.out[j];   // add to perm new link
            }
        } else {
            isDel = false;
        }
    }
    return true;
}


// check new placement of topLevel
 bool testPlacement(vector <Node> &graph, vector <Node> &subgraph, vector <int> &topLevel, vector <int> &placement, vector <int> &ans)  // test new placement
 {
    map <int, int> perm;    // index of Node in graph<->subgraph (permutation)
    vector <int> checked(placement.size());  // nodes nedd to check
    int i, j;
    bool isDel;
    for (i = 0; i < topLevel.size(); i++) {
        perm[placement[i]] = topLevel[i];
        checked[i] = placement[i];
    }

    while (checked.size()) {
        for (i = 0; i < checked.size(); i++) {
             Node current = graph[checked[i]];
             Node currentSub = subgraph[perm[checked[i]]];
             if (current.out.size() != currentSub.out.size()) {
                 return false;
             }
             if (!checkChilds(graph, subgraph, current, currentSub, perm, checked, isDel)) {
                 return false;
             }
             if (isDel) {
                 checked.erase(checked.begin() + i);
                 i--;
             }
        }
    }


    ans.clear();
    ans.resize(subgraph.size());
    map<int, int>:: iterator t;

    // set IN's
    Node x, xs;
    for (t = perm.begin(); t != perm.end(); ++t) {
        x = graph[t->first];
        xs = subgraph[t->second];
        if (subgraph[xs.in[0]].type == IN) {
            perm[x.in[0]] = xs.in[0];
        }

        if (subgraph[xs.in[1]].type == IN) {
            perm[x.in[1]] = xs.in[1];
        }


    }

    for (t = perm.begin(); t != perm.end(); ++t) {
        ans[t->second] = t->first;
    }
 }

 // find place of subgraph
 // res - vector of indexes, mean that node i in subgraph placed by node res[i] in graph
bool findSubGraph(vector <Node> &graph, vector<Node> &subgraph, vector<int> &answer)
{
    vector <int> topLevel;
    findTopLevel(subgraph, topLevel);
    vector<int> placement(topLevel.size(), -1);
    if (!initPlacement(graph, subgraph, topLevel, placement)) {    // we can't find one of the element in subgraph in graph -> false
        return false;
    }
   do {
        if (testPlacement(graph, subgraph, topLevel, placement, answer)) {
            return true;
        }
    } while (getPlacement(graph, subgraph, topLevel, placement));
    return false;
}


int main(int args, char *argv[])
{
    /*vector<Node> graph, subgraph;
    vector <int> res;
    findSubGraph(graph, subgraph, res);*/
    return 0;
}


