#ifndef PARSER_HPP_
#define PARSER_HPP_

#include <iostream>
#include <cstring>
#include <cstdio>
#include <vector>


using namespace std;

template < class T, int max_size >

class Stack {
	T  s [max_size];
	int top;
public:
		Stack () { top = 0; }
		void reset () { top = 0; }
		void push ( T i ){
				if ( !is_full() ) s [top++] = i;
				else throw "Stack_is_full";
		}
		T pop (){
			if ( !is_empty() ) return s[--top];
			else throw "stack_is_empty";
		}
		bool is_empty() { 
			return top == 0; }
		bool is_full() { 
			return top == max_size; }
		T get_top(){
            if (!is_empty()) return s[top-1];
            else throw "STACK is empty";
        }
        T& operator[] ( int index ) {
		if (index > top)
			throw "STACK:out of array";
		else
			return s[index];
		}
		void print() {
			T a;
			for ( int i = 0; i < top; ++i ){
		    a=s[i];
		    cout << a<<endl;
			}
		}
};

enum type_of_lex{
    LEX_OR,   			//0
    LEX_NOR,   			//1
    LEX_XNOR,   		//2
    LEX_AND,     		//3
    LEX_NAND,			//4
    LEX_XOR,    		//5
    LEX_NULL,   		//6
    LEX_SEMICOLON,		//7
    LEX_LBRACKET,  		//8
    LEX_RBRACKET,   	//9
    LEX_MODULE,			//10
    LEX_COMMA,			//11
    LEX_INPUT,			//12
    LEX_OUTPUT,			//13
    LEX_COLON,			//14
    LEX_WIRE,			//15
    LEX_REG,			//16
    LEX_ENDMODULE,		//17
    LEX_ID,				//18
    LEX_NUM,			//19
    LEX_LSQBRACKET,		//20
	LEX_RSQBRACKET,		//21
	LEX_ASSIGN,			//22
	LEX_NOT				//23
};

class Lex{
	type_of_lex t_lex;
	float v_lex;
	char* str_lex;
	public:
		Lex(type_of_lex t=LEX_NULL, float v=0);
		Lex( type_of_lex t, float v, char * s);
		type_of_lex get_type();
		float get_val();
		char * get_str();
		friend ostream& operator << (ostream &s, Lex l){
			s<<'('<<l.t_lex<<','<<l.v_lex<<");";
			return s;
		}
};

class Scanner{
	enum state{H,WORD,NUMB};
	static const char *TW[];
	static type_of_lex words[];
	static char c;
	state CS;
	FILE * fp;
	char buf[256];
	int buf_top;

	void clear();
	void add();
	int look (char *buf, const char**list);
	void gc()
	{
		c=fgetc(fp);
    }
public:
	Scanner(const char* program);
	Lex get_lex();
};

char Scanner::c=' ';


class Ident{
	char * name;
	type_of_lex type;
	float value;
	string str;
	bool assign;
	bool declare;
	bool fname;
public:
    Ident(){
        assign=false;
        declare=false;
        fname=false;
    }
	/*const*/ char *get_name(){
		//cout<<")))"<<endl;
		return name;
	}
	void put_name(const char *n){
		name = new char [strlen(n)+1];
		strcpy(name,n);
	}
	type_of_lex get_type(){
		return type;
	}
	void put_type(type_of_lex t){
		type=t;
	}
	float get_val(){
		return value;
	}

	void put_val(float v){
		value=v;
	}
	bool get_assign(){
	    return assign;
	}
	void put_assign(){
	    assign=true;
	}
	bool get_declare(){
	    return declare;
	}
	void put_declare(){
	    declare=true;
	}
    string get_str(){
        return str;
    }
    bool get_fname(){
        return fname;
    }
    void put_str(string s){
        str=s;
        return;
    }
    void put_fname(){
        fname=true;
    }
};


class tabl_ident{
	Ident *p;
	int size;
	int top;

public:

	tabl_ident(int max_size){
		p=new Ident[size=max_size];
		top=1;
	}

	~tabl_ident(){
		delete []p;
	}

	Ident &operator[] (int k){
		return p[k];
	}
	int put(/*const*/ char *buf);
	
	void print() {
			Ident a;
			for ( int i = 0; i < top; ++i ){
		    a=p[i];
		    //printf("%s\n",p[i].get_name());
		    //cout <<"jj"<< a.get_name()<<"oo"<<endl;;
			}
		}
	

};


class Poliz {
	Lex *p;
	int size;
	int free;
public:

	Poliz ( int max_size ) {
		p = new Lex [size = max_size];
		free = 0;
	};

	~Poliz() { delete []p; };

	void put_lex(Lex l) { p[free]=l; ++free; };
	void put_lex(Lex l, int place) { p[place]=l; };
	void blank() { ++free; };
	int get_free() { return free; };

	Lex& operator[] ( int index ) {
		if (index > size)
			throw "POLIZ:out of array";
		else
		if ( index > free )
			throw "POLIZ:indefinite element of array";
		else
			return p[index];
	};

	void print() {
		Lex a;
		for ( int i = 0; i < free; ++i )
		{
		    a=p[i];
		    cout << a<<endl;
		}
	};
};


class Parser{
	Lex cur_lex;
    type_of_lex c_type;
    float c_val;
    Stack < int, 100000 > st_int;
	Stack < type_of_lex, 10000> st_lex;
    Scanner scan;
	void start();
	void gl();
	void brackets();
	void descr();
	void descrgraph();
	int Nand;
	int Nnand;
	int Nor;
	int Nnor;
	int Nxor;
	int Nxnor;
	int weight;
	public:
		Poliz prog;
		Parser(const char* program):scan(program),prog(1000),Nand(0),Nnand(0),Nor(0),Nnor(0),Nxor(0),Nxnor(0),weight(0){
			cout<<"ll";};
		void begin();
		void parse();
};

class Node{
	type_of_lex type;
	int in[2];  
	int out;
	public:
		Node();
		void put_out(int a);
		void put_in0(int a);
		void put_in1(int a);
		void put_type(type_of_lex a);
};

class Graph{
	public:
		vector<Node> element;
		Graph(int a){
			element.resize(a);
		}
		Graph(Graph& g){
			element.resize(g.element.size());
			for (int i = 0; i < element.size();i++) element[i] = g.element[i];
		}
};

#endif
