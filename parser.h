#ifndef PARSER_H_
#define PARSER_H_

#include <iostream>
#include <cstring>
#include <cstdio>
#include "parser.h"

using namespace std;

enum type_of_lex{
	LEX_NULL,   		//0
    LEX_SEMICOLON,    	//1
    LEX_OR,   			//2
    LEX_NOR,   			//3
    LEX_XNOR,   		//4
    LEX_AND,     		//5
    LEX_NAND,			//6
    LEX_XOR,    		//7
    LEX_LBRACKET,  		//8
    LEX_RBRACKET,   	//9
    LEX_MODULE,			//10
    LEX_COMMA,			//11
    LEX_INPUT,			//12
    LEX_OUTPUT,			//13
    LEX_COLON,			//14
    LEX_WIRE,			//15
    LEX_REG,			//16
    LEX_ENDMODULE,		//17
    LEX_ID
};

class Lex{
	type_of_lex t_lex;
	float v_lex;
	string str_lex;
	public:
		Lex(type_of_lex t=LEX_NULL, float v=0);
		Lex( type_of_lex t, float v, string s);
		type_of_lex get_type();
		float get_val();
		string get_str();
		friend ostream& operator << (ostream &s, Lex l){
			s<<'('<<l.t_lex<<','<<l.v_lex<<");";
			return s;
		}
};

class Scanner{
	enum state{H,WORD};
	static const char *TW[];
	static type_of_lex words[];
	static char c;
	state CS;
	FILE * fp;
	char buf[256];
	int buf_top;

	void clear();
	void add();
	int look (char *buf, const char**list);
	void gc()
	{
		c=fgetc(fp);
    }
public:
	Scanner(const char* program);
	Lex get_lex();
};

char Scanner::c=' ';

class Parser{
	Lex cur_lex;
    type_of_lex c_type;
    float c_val;
    Scanner scan;
	void start();
	void gl();
	public:
		Parser(const char* program):scan(program){}
};

#endif
