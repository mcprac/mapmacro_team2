#include "parser.hpp"

using namespace std;

tabl_ident TID(100);
vector<Node> graph;
int graphcount=0;

/////////////////////////////
////Lexer
Lex::Lex (type_of_lex t, float v){
			t_lex=t; v_lex=v;
		}
		
Lex::Lex( type_of_lex t, float v, char * s){
			t_lex = t;
			v_lex = v;
			str_lex = s;
		}
		
type_of_lex Lex::get_type(){
			return t_lex;
		}
		
float Lex::get_val(){
			return v_lex;
		}
		
char * Lex::get_str() { 
			return str_lex; 
		}

		
const char* Scanner::TW[]={
		"",					//0
		";",				//1
		"or",				//2
		"nor",				//3
		"xnor",				//4
		"and",				//5
		"nand",				//6
		"xor",				//7
		"(",				//8
		")",				//9
		"module",			//10
		",",				//11
		"input",			//12
		"output",			//13
		":",				//14
		"wire",				//15
		"reg",				//16
		"endmodule",		//17
		"[",				//18
		"]",				//19
		"assign",			//20
		"not",				//21
		NULL
};

type_of_lex Scanner::words[]={
		LEX_NULL,   		//0
		LEX_SEMICOLON,    	//1
		LEX_OR,   			//2
		LEX_NOR,   			//3
		LEX_XNOR,   		//4
		LEX_AND,     		//5
		LEX_NAND,			//6
		LEX_XOR,   			//7
		LEX_LBRACKET,  		//8
		LEX_RBRACKET,  		//9
		LEX_MODULE,			//10
		LEX_COMMA,			//11
		LEX_INPUT,			//12
		LEX_OUTPUT,			//13
		LEX_COLON,			//14
		LEX_WIRE,			//15
		LEX_REG,			//16
		LEX_ENDMODULE,		//17
		LEX_LSQBRACKET,		//18
		LEX_RSQBRACKET,		//19
		LEX_ASSIGN,			//20
		LEX_NOT,			//21
		LEX_NULL
};

Scanner::Scanner ( const char * program ){
		fp = fopen ( program, "r" );
		CS = H;
		clear();
		gc();
}

void Scanner::clear(){
	buf_top=0;
	for (int j=0;j<256;++j)
		buf[j]='\0';
}

void Scanner::add(){
	buf [buf_top++]=c;
}

int Scanner::look( char *buf, const char**list){
	int i=0;
	while (list[i]){
		if (!strcmp(buf,list[i]))
			return i;
		i++;
	}
	return 0;
}

Lex Scanner::get_lex(){
	CS=H;
	int j,d=0;
	do{
		switch(CS){
			case H:
				if ((c==' ') || (c=='\t') || (c=='\n')) gc();
				else if (isalpha(c)){
						clear();
						add();
						//cout<<c<<endl;
						gc();
						CS=WORD;
					}
					else if (isdigit(c)){
							d=c-'0';
							gc();
							CS=NUMB;
						}
					else{
						clear();
						add();
						//cout<<c<<endl;
						j=look(buf,TW);
						gc();
						if (j) return Lex(words[j],j);
						else throw "Unexpected symbol";
					}
            break;
            case WORD:
				if (isalpha(c)||isdigit(c) || (c == '[') || (c == ']')|| (c == '_')){
					add();
					//cout<<c<<endl;
					gc();
				}
				else{
					j=look(buf,TW);
					if (j) return Lex(words[j],j);
					else{
						strcat(buf,"\0");
						j=TID.put(buf);
						return Lex(LEX_ID,j);
					}
				}
			break;
			case NUMB:
			if(isdigit(c)){
				d=d*10+(c-'0');
				gc();
			}
			else return Lex(LEX_NUM,d);
			break;
                
		}
	}
	while(1);
}


int tabl_ident::put( char *buf){
	for (int j=1;j<top;j++){
		if( !strcmp(buf,p[j].get_name()))
			return j;
	}
	p[top].put_name(buf);
	++top;
	return top-1;
}


void Node::put_type(type_of_lex a){
	type = a;
}

void Node::put_in0(int a){
	in[0]=a;
}

void Node::put_in1(int a){
	in[1]=a;
}

void Node::put_out(int a){
	out=a;
}

Node::Node(){
	type = LEX_NULL;
	in[0]=0;
	in[1]=0;
	out=0;
}

//////////////////////////////
////Parser
void Parser::begin(){
	cout<<"gg";
	start();
	cout<<endl<<"Analyze is successfull!"<<endl;
	cout<<"AND"<<"   "<<"NAND"<<"   "<<"NOR"<<"   "<<"OR"<<"   "<<"XOR"<<"   "<<"XNOR"<<"   "<<"SCORE"<<endl;
	cout<<Nand<<"     "<<Nnand<<"     "<<Nnor<<"     "<<Nor<<"     "<<Nxor<<"     "<<Nxnor<<"     "<<weight<<endl;
    //cout << endl<<"POLIZ"<<endl;
	//prog.print();
    //cout << endl<<"LEX"<<endl;
	//st_lex.print();
}

void Parser::start(){
	cout<<"kk";
	gl();
	if (c_type != LEX_MODULE) throw " MODULE expected";
	else{
		cout<<"oo";
			prog.put_lex(LEX_MODULE);
			gl();
			if (c_type != LEX_ID) throw "name of module expected";
			else{
					st_lex.push(LEX_ID);
					prog.put_lex(LEX_ID);
					gl();
					if (c_type != LEX_LBRACKET) throw "no arguments in module";
					else{
							brackets();//обрабатывает всё внутри скобок
							if (c_type != LEX_RBRACKET) throw "closing right bracket expected";
							else{
								gl();
								if (c_type != LEX_SEMICOLON) throw "semicolon expected";
								else{
									gl();
									cout<<"hh";
									descr();//обработка описания
									descrgraph();//описание схемы
									//gl();
									//cout<<cur_lex<<endl;
									if (c_type != LEX_ENDMODULE) throw "ENDMODULE expected";
									prog.put_lex(LEX_ENDMODULE);
									cout<<"nnn"<<endl;
								}
							}
					}
			}
	}
}

void Parser::gl(){
	cur_lex=scan.get_lex();
	c_type=cur_lex.get_type();
	c_val=cur_lex.get_val();
}

void Parser::brackets(){
	gl();
	if (c_type != LEX_ID) throw "ID expected 1";
	else{
		st_lex.push(LEX_ID);
		prog.put_lex(LEX_ID);
		st_int.push(c_val);
		gl();
		}
	while (c_type == LEX_COMMA){
			gl();
			if (c_type != LEX_ID) throw "ID expected 2";
			else{
				st_lex.push(LEX_ID);
				prog.put_lex(LEX_ID);
				st_int.push(c_val);
				gl();
				if(c_type!=LEX_COMMA && c_type!= LEX_RBRACKET) throw ") expexted";
			}
	}
}

void Parser::descr(){
	int a=0,b=0;
	if((c_type == LEX_INPUT) || (c_type == LEX_OUTPUT) 
	|| (c_type == LEX_WIRE) || (c_type == LEX_REG)){
		st_lex.push(c_type);
		prog.put_lex(c_type);
		gl();
		if (c_type == LEX_LSQBRACKET){
			gl();
			if (c_type !=LEX_NUM) throw "number expected";
			a=c_val;
			//cout<<a<<endl;
			gl();
			if (c_type != LEX_COLON) throw "colon expected";
			gl();
			if (c_type !=LEX_NUM) throw "number expected";
			b=c_val;
			//cout<<b<<endl;
			if (a>b){
				int c;
				c=b;
				b=a;
				a=c;
			}
			gl();
			if (c_type != LEX_RSQBRACKET) throw "] expected";
			gl();
		}
		//cout <<cur_lex<<endl;
		//gl();
		//out <<cur_lex<<endl;
		if (c_type != LEX_ID) throw "ID expected 3";
		else{
			if (a==b){
				st_lex.push(LEX_ID);
				prog.put_lex(LEX_ID);
				st_int.push(c_val);
			}
			else{
				char help[100];
				int h=c_val;
				strcpy(help,TID[c_val].get_name());
				strcat(TID[c_val].get_name(),"[\0");
				char buffer[100];
				sprintf(buffer, "%d", a);
				strcat(TID[c_val].get_name(),buffer);
				strcat(TID[c_val].get_name(),"]\0");
				//cout<<TID[c_val].get_name()<<endl;
				st_lex.push(LEX_ID);
				prog.put_lex(LEX_ID);
				for (int i=a+1;i<=b;i++){
					char help1[100];
					strcpy(help1,help);
					strcat(help1,"[\0");
					char buffer[100];
					sprintf(buffer, "%d", i);
					strcat(help1,buffer);
					strcat(help1,"]\0");
					//cout<<help1<<endl;
					TID.put(help1);
					h++;
					st_lex.push(LEX_ID);
					prog.put_lex(LEX_ID);
				}
				//надо дописать!! запушить в стек массив из d эл-тов с названием тек. ID
			}
			gl();
		}
		while (c_type == LEX_COMMA){
			gl();
			if (c_type != LEX_ID) throw "ID expected 4";
			else{
				if (a==b){
				st_lex.push(LEX_ID);
				prog.put_lex(LEX_ID);
				st_int.push(c_val);
				}
				else{
					for (int i=a;i<=b;i++){
						strcat(TID[c_val].get_name(),"[\0");
						char buffer[100];
						sprintf(buffer, "%d", i);
						strcat(TID[c_val].get_name(),buffer);
						strcat(TID[c_val].get_name(),"]\0");
						//cout<<TID[c_val].get_name()<<endl;
						st_lex.push(LEX_ID);
						prog.put_lex(LEX_ID);//REWRITE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1
					}
				}
				gl();
				if(c_type!=LEX_COMMA && c_type!= LEX_SEMICOLON) throw "; expexted";
				//gl();
			}
		}
		gl();
		descr();
	}
	else descrgraph();
}

void Parser::descrgraph(){
	if((c_type == LEX_NAND) || (c_type == LEX_NOR) 
	|| (c_type == LEX_AND) || (c_type == LEX_OR) 
	|| (c_type == LEX_XNOR) ||(c_type == LEX_XOR)){
		st_lex.push(c_type);
		prog.put_lex(c_type);
		if (c_type == LEX_NAND) {Nnand++;weight+=2;}
		if (c_type == LEX_AND) {Nand++;weight+=2;}
		if (c_type == LEX_NOR) {Nnor++;weight+=2;}
		if (c_type == LEX_OR) {Nor++;weight+=2;}
		if (c_type == LEX_XNOR) {Nxnor++;weight+=6;}
		if (c_type == LEX_XOR) {Nxor++;weight+=6;}
		Node graph[graphcount];
		graph[graphcount].put_type(c_type);
		gl();
		if (c_type == LEX_LBRACKET){
			gl();
			if (c_type != LEX_ID) throw "wrong 1 argument";
			else{
				st_lex.push(LEX_ID);
				prog.put_lex(LEX_ID);
				graph[graphcount].put_out(c_val);
				st_int.push(c_val);
				gl();
				if (c_type != LEX_COMMA) throw "no comma";
				else{
					gl();
					if (c_type != LEX_ID) throw "wrong 2 argument";
					else{
						st_lex.push(LEX_ID);
						prog.put_lex(LEX_ID);
						graph[graphcount].put_in0(c_val);
						st_int.push(c_val);
						gl();
						if (c_type != LEX_COMMA) throw "no comma";
						else{
							gl();
							if (c_type != LEX_ID) throw "wrong 3 argument";
							else{
								st_lex.push(LEX_ID);
								prog.put_lex(LEX_ID);
								graph[graphcount].put_in0(c_val);
								st_int.push(c_val);
								graphcount++;
								gl();
								if (c_type != LEX_RBRACKET) throw "no )";
								else{
									gl();
									if (c_type != LEX_SEMICOLON) throw "no semicolom";
									gl();
									descrgraph();
								}
							}
						}
					}
				}
			}
		}
		else throw "no argumentssssssss";
	}/*
	if (c_type == LEX_NOT){
		//st_lex.push(c_type);
		cout<<"7";
		//prog.put_lex(c_type);
		//Node graph[graphcount];
		//graph[graphcount].put_type(c_type);
		cout<<"9";
		gl();
		cout<<"6";
		//cout<<c_type;
		if (c_type == LEX_LBRACKET){
			gl();
			cout<<"33";
			if (c_type != LEX_ID) throw "wrong 1 argument";
			else{
				st_lex.push(LEX_ID);
				prog.put_lex(LEX_ID);
				graph[graphcount].put_out(c_val);
				st_int.push(c_val);
				gl();
				if (c_type != LEX_COMMA) throw "no comma";
				else{
					gl();
					if (c_type != LEX_ID) throw "wrong 2 argument";
					else{
						st_lex.push(LEX_ID);
						prog.put_lex(LEX_ID);
						graph[graphcount].put_in0(c_val);
						st_int.push(c_val);
						gl();
						if (c_type != LEX_RBRACKET) throw "no )";
						else{
								gl();
								if (c_type != LEX_SEMICOLON) throw "no semicolom";
								gl();
								descrgraph();
							}
						}
					}
				}
			}
		}*/
}

void Parser::parse(){
	try{
		begin();
	}
	catch(const char* l){
            cout<<l<<endl;
    }
    catch(...){
            cout<<"Error!";
    }
}

int main(){
	Parser parser("design.v");
	cout<<"jj";
	parser.parse();
	return 0;
}
